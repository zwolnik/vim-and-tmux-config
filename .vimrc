set nocompatible                "disable vi compatibility

"vim-plug autoinstall
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"General configuration
set number		        "line numbers
set relativenumber	        "and relative ones
set history=1000	        "more history
set showcmd		        "show incomplete cmds down the bottom
set showmode		        "show current mode down the bottom
set ruler                       "add bottom statusline
set visualbell		        "disable soundbell
set autoread		        "reload files outside of vim
set hidden		        "allow buffers to exist in background
set showmatch                   "show matching brackets
set mouse=a                     "enable mouse
set ttymouse=xterm2             "enable mouse tracking via xterm
set nobackup                    "disable backup
set noswapfile                  "disable swap files creation
set backspace=indent,eol,start  "proper backspacing
set splitbelow                  "normal splitting
set splitright                  "same as above

"Searching
set incsearch		"searching in typing time
set hlsearch		"highlight matches
set ignorecase		"case insensitive
set smartcase		"unless capital is typed

"Indentation
set expandtab		"replace tab with spaces
set softtabstop=4	"width of tab
set shiftwidth=4	"need to be same as above
set smartindent		"smart indenting for C-like files

"Scrolling
set nowrap              "do not wrap lines
set scrolloff=5         "scroll when x lines from bottom
set sidescrolloff=10    "scroll when y columns from side
set sidescroll=1        "scroll vertically by y col

"Statusline
set laststatus=2                    "always show statusbar

"Folding
set foldmethod=indent   "fold according to indent
set foldnestmax=10
set nofoldenable        "normal files upon opening
set foldlevel=2

"Keymaping
"disable arrows
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

"movement
noremap d h
noremap h j
noremap t k
noremap n l

"splits navigation
nnoremap <C-H> <C-W><C-J>
nnoremap <C-T> <C-W><C-K>
nnoremap <C-N> <C-W><C-L>
nnoremap <C-D> <C-W><C-H>

"delete
noremap  k d
"next search
nnoremap m n
"prev search
nnoremap M N
"easier commands
noremap  s :
"map esc in insert/replace mode to tn
inoremap hk <ESC>

"return to last edit position when opening files
augroup last_edit
  autocmd!
  autocmd BufReadPost *
       \ if line("'\"") > 0 && line("'\"") <= line("$") |
       \   exe "normal! g`\"" |
       \ endif
augroup END

"Plugins
call plug#begin('~/.vim/plugged')

Plug 'ErichDonGubler/vim-sublime-monokai'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'rust-lang/rust.vim'
Plug 'liuchengxu/eleline.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'scrooloose/nerdcommenter'
Plug 'ron-rs/ron.vim'
Plug 'cespare/vim-toml'
"Plug 'ycm-core/YouCompleteMe'

call plug#end()

" --------------------- Theme
syntax on
colorscheme sublimemonokai

" --------------------- CTRL-P configuration
let g:ctrlp_map = '<c-p>'
let g:ctrlp_use_caching = 1
let g:ctrlp_cache_dir = '~/.cache/ctrlp'
let g:ctrlp_max_files=0         "no file limit

" bind opening choosen file to 't' normal 'h' hsplit 'v' vsplit
" removed <c-h> from PrtCurLeft to not collide 
let g:ctrlp_prompt_mappings = {
    \'AcceptSelection("t")': ['<c-t>'], 
    \'AcceptSelection("h")': ['<c-h>'],
    \'AcceptSelection("v")': ['<c-v>'],
    \'PrtCurLeft()':         ['<left>', '<c-^>'],
\}

" force usage of ripgrep by ctrl-p if present
if executable('rg')
  set grepprg=rg
  let g:ctrlp_user_command = 'rg %s --files --glob ""'
endif

" --------------------- NERD Tree
" Open nerdtree automatically when using 'vim <dirname>'
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" Toggling nerdtree
map <C-e> :NERDTreeToggle<CR>
" Open in tab - 't'
let NERDTreeMapOpenInTab='<c-t>'
let NERDTreeMapOpenSplit='<c-h>'
let NERDTreeMapOpenVSplit='<c-v>'

" Close vim if the only window left is nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" --------------------- NERD Commenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" You Complete Me
"let g:ycm_key_invoke_completion = '<C-Space>'
"let g:ycm_enable_diagnostic_signs = 1
"let g:ycm_enable_diagnostic_highlighting = 0
"let g:ycm_error_symbol = '✘'
"let g:ycm_warning_symbol = '▲'

" --------------------- Netrw remaping
augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup END

function! NetrwMapping()
    noremap <buffer> d h
    noremap <buffer> h j
    noremap <buffer> t k
    noremap <buffer> n l
    noremap <buffer> s :
endfunction

" ----------------------- CoC
" if hidden is not set, TextEdit might fail.
set hidden
" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup
" Better display for messages
set cmdheight=2
" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300
" don't give |ins-completion-menu| messages.
set shortmess+=c
" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p :<C-u>CocListResume<CR>
